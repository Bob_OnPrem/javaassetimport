package com.onprem.opentext;


import com.artesia.asset.Asset;
import com.artesia.asset.AssetContentInfo;
import com.artesia.asset.imprt.ImportAsset;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.content.ContentInfo;
import com.artesia.content.RenditionContentInfo;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataField;
import com.artesia.security.SecurityPolicy;
import com.artesia.security.SecuritySession;
import com.artesia.security.services.SecurityPolicyServices;
import com.onprem.opentext.services.AssetImportService;
import com.onprem.opentext.services.AuthSessionService;
import com.onprem.opentext.tempconfig.OtmmConfig;
import fj.data.Either;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class App {

    private static final String SECURITY_POLICY = "Policy for Everyone";
    private static final String FILE_TO_UPLOAD = "C:\\Users\\Administrator\\Desktop\\JAssetImportTest\\Anchorman.png";

    private static SecuritySession securitySession;

    public static void main(String[] args) {
        startAssetUpload();
    }

    public static void startAssetUpload(){
        OtmmConfig.setPathMediaManager();

        boolean authSuccess = AuthSessionService.authenticate(OtmmConfig.getUsername(), OtmmConfig.getPassword());
        if(authSuccess){
            securitySession = AuthSessionService.getSession();

            //attempt asset import
            Either<Boolean,List<ImportAsset>> importEither = createAssetList();
            if(importEither.isRight()){
                AssetImportService.importAsset(importEither.right().value(), securitySession);
            }
        }
    }


    private static Either<Boolean, List<ImportAsset>> createAssetList(){
        ArrayList<ImportAsset> assetList = new ArrayList<>();

        Either<Boolean,SecurityPolicy> policyEither = createSecurityPolicy(securitySession);
        if(policyEither.isLeft()) {
            return Either.left(false);
        }

        Asset asset = createAsset(policyEither);
        ImportAsset imp = new ImportAsset(null, asset);
        imp.setProxyContentMimeType("image/png");
        assetList.add(imp);

        return Either.right(assetList);
    }

    //TODO THIS METHOD HAS HARDCODED DATA
    private static Asset createAsset(Either<Boolean,SecurityPolicy> policyEither){
        Asset asset = new Asset();
        asset.setMetadataModelId(new TeamsIdentifier("ARTESIA.MODEL.DEFAULT"));
        AssetContentInfo contentInfo = new AssetContentInfo();
        File testFile = new File(FILE_TO_UPLOAD);
        ContentInfo masterContent = new ContentInfo(testFile);
        contentInfo.setMasterContent(masterContent);
        asset.setAssetContentInfo(contentInfo);
        asset.setName("test");
        asset.addSecurityPolicy(policyEither.right().value());

        //TODO rendition info could be added here

        addMetadata(asset);
        addRenditionContent(asset);
        return asset;
    }

    private static void addRenditionContent(Asset asset){
        RenditionContentInfo renditionInfo = new RenditionContentInfo();
        // Set the image to use as the thumbnail
        ContentInfo thumb = new ContentInfo(new File("FILE_TO_UPLOAD"));
        thumb.setMimeType("image/jpeg");
        renditionInfo.setThumbnailContent(thumb);

        asset.setRenditionContent(renditionInfo);
    }


    private static void addMetadata(Asset asset){
        MetadataCollection mc = new MetadataCollection();
        MetadataField descrField = new MetadataField(new TeamsIdentifier("ARTESIA.FIELD.DESCRIPTION"));
        descrField.setValue("This is Ron Burgundy.  He's the best Anchonman in all the land");
        MetadataField authorField = new MetadataField(new TeamsIdentifier("ARTESIA.FIELD.DOCUMENT AUTHOR"));
        authorField.setValue("Veronica Corningstone");

        mc.addMetadataElement(descrField);
        mc.addMetadataElement(authorField);

        asset.setMetadata(mc);
    }

    private static Either<Boolean, SecurityPolicy> createSecurityPolicy( SecuritySession securitySession){
        try {
            SecurityPolicy sp = SecurityPolicyServices.getInstance().retrieveSecurityPoliciesByName(SECURITY_POLICY, securitySession)[0];
            return Either.right(sp);
        } catch (BaseTeamsException e){
            // not great for production
            return Either.left(false);
        }
    }
}
