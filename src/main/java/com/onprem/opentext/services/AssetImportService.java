package com.onprem.opentext.services;


import com.artesia.asset.imprt.ImportAsset;
import com.artesia.asset.imprt.ImportJob;
import com.artesia.asset.imprt.services.AssetImportServices;
import com.artesia.bpm.ProcessDefinitionId;
import com.artesia.bpm.ProcessInstanceId;
import com.artesia.bpm.context.ImportJobProcessContext;
import com.artesia.bpm.context.ProcessContextFactory;
import com.artesia.bpm.services.BusinessProcessServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.common.io.FileUtils;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.manageddirectory.ManagedDirectory;
import com.artesia.manageddirectory.services.ManagedDirectoryServices;
import com.artesia.security.SecuritySession;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class AssetImportService {

    public static void importAsset(List<ImportAsset> assetList, SecuritySession securitySession) {
        // create import job, which internally creates managed_directory
        try {

            // THIS IS AN OPTIONAL STEP WHICH CREATES A MANAGED DIRECTORY.  IM NOT SURE WE WANT THIS.  DOES
            // THE FILE GET COPIED TO THE MANAGED DIRECTORY LOCATION, OR DOES THE FILE LOCATION BECOME THE MANAGED DIRECTOY
            // THAT GETS CLEANED UP AFTERWARDS.  FORMER -> BAD, LATTER -> GOOD
            ImportJob job = AssetImportServices.getInstance().createImportJob(securitySession);
            job.setImportAssetList(assetList);
            // ** job.getImportJobId() doubles as the managed directory id!

            // create import context
            ImportJobProcessContext importJobProcessContext = configureImportJobContext(job, securitySession);

            // Retreieve process definition id and initiate a job
            BusinessProcessServices service = BusinessProcessServices.getInstance();

            ProcessDefinitionId processDefinitionId = service.retrieveProcessDefinitionByName(job.getImportJobId().getId(), importJobProcessContext, securitySession).getId();
            ProcessInstanceId processInstanceId = service.instantiateProcess(securitySession, processDefinitionId, importJobProcessContext, true).getProcessInstanceId();
            ManagedDirectory managedDirectory = ManagedDirectoryServices.getInstance().retrieveManagedDirectory(new TeamsIdentifier(job.getImportJobId().getTeamsId()), securitySession);

            //TODO this == bad.  Pulling the same file back out, but it's how the example works.  Let's prove it out first
            File testFile = assetList.get(0).getAsset().getAssetContentInfo().getMasterContent().getFile();

            ManagedDirectoryServices.getInstance().stageContent(FileUtils.getByteArrayFromFilePath(testFile.getPath()), testFile.getName(), managedDirectory.getManagedDirectoryId(), securitySession);

            // update managed directory with process instance id & set content_staged as true. This is mandatory.
            // until set as true the process waits at waitAssetstobeStagedAgent step. This is mandatory.
            managedDirectory.setProcessInstanceId(processInstanceId);
            managedDirectory.setContentStaged(true);
            managedDirectory.setContentCleanup(true);

            // update ManagedDirectory
            ManagedDirectoryServices.getInstance().updateManagedDirectory(managedDirectory, securitySession);
        } catch (BaseTeamsException e){
            System.out.println("Fail! " + e);
        } catch (IOException e){
            System.out.println("Fail! " + e);
        }
    }

    private static ImportJobProcessContext configureImportJobContext(ImportJob job, SecuritySession securitySession) {
        ImportJobProcessContext context = ProcessContextFactory.getInstance().createContext(ImportJobProcessContext.class, securitySession);
        context.setImportJob(job);
        return context;
    }

}
