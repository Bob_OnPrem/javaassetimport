package com.onprem.opentext.services;


import com.artesia.common.exception.BaseTeamsException;
import com.artesia.security.SecuritySession;
import com.artesia.security.session.services.AuthenticationServices;

public class AuthSessionService {

    //THIS DOESN'T SCALE
    private static SecuritySession session;

    public static boolean authenticate(String username, String password) {
        try {
            session = AuthenticationServices.getInstance().login(username, password);
            return true;
        } catch(BaseTeamsException e){
          return false;
        }
    }

    public static SecuritySession getSession(){
        return session;
    }
}
