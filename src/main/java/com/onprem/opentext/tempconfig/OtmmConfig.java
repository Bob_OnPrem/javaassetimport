package com.onprem.opentext.tempconfig;


import java.util.Properties;

public class OtmmConfig {
    // TODO entire class would be replaced with actual config files and encrypted passwords
    private static final String PATH_MEDIA_MANAGER = "TEAMS_HOME";
    private static final String PATH_MEDIA_MANAGER_PATH = "/Users/Bob/MediaManager";
    private static final String USER_NAME = "tsuper";
    private static final String PASSWORD = "q5pUYUPs9_5eERDm";

    public static void setPathMediaManager() {
        Properties system = System.getProperties();
        system.setProperty(PATH_MEDIA_MANAGER, PATH_MEDIA_MANAGER_PATH);
        System.setProperties(system);
    }

    public static String getUsername() {
        return USER_NAME;
    }

    public static String getPassword() {
        return PASSWORD;
    }
}
