#!/bin/ksh

#THESE ENVIRONMENT VARIABLES SHOULD ALREADY BE SET: TEAMS_HOME, JAVA_HOME, JBOSS_HOME - or you can set them below
#TEAMS_HOME=
#JAVA_HOME=
#JBOSS_HOME=

#----------------------------------------------------------

if [ -z "$TEAMS_HOME" ]; then
   echo TEAMS_HOME is not set.
   exit 1
fi

if [ -z "$JAVA_HOME" ]; then
   echo JAVA_HOME is not set.
   exit 1
fi

if [ -z "$JBOSS_HOME" ]; then
   echo JBOSS_HOME is not set.
   exit 1
fi

TEAMS_LIB=$TEAMS_HOME/jars

RUNCMD=$JAVA_HOME/bin/java
RUNCMD="$RUNCMD  -agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=5005"
RUNCMD="$RUNCMD -classpath ./target/JAssetImport-1.0-SNAPSHOT.jar:$TEAMS_LIB/TEAMS-sdk.jar:$TEAMS_LIB/TEAMS-common.jar:$TEAMS_LIB/commons-io-1.4.jar:$TEAMS_LIB/Regexp.jar:$TEAMS_LIB/xml4j_2_0_15.jar:$TEAMS_LIB/commons-logging-1.1.1.jar:$TEAMS_LIB/commons-beanutils-1.8.0.jar:$TEAMS_LIB/commons-lang-2.4.jar:$TEAMS_LIB/jdom.jar:$TEAMS_LIB/activation.jar:$JBOSS_HOME/client/jbossall-client.jar"
RUNCMD="$RUNCMD -DTEAMS_HOME=$TEAMS_HOME"
RUNCMD="$RUNCMD -jar target/JAssetImport-1.0-SNAPSHOT.jar"

echo Running command $RUNCMD $*

${RUNCMD} $*